import {Builder} from 'selenium-webdriver';
import {Options as ChromeOptions} from 'selenium-webdriver/chrome';
import {Options as FirefoxOptions} from 'selenium-webdriver/firefox';
import {WebDriverWrapper} from './web-driver.wrapper';

export class WebDriverFactory {
	async createInsecureChrome(): Promise<WebDriverWrapper> {
		const options = new ChromeOptions()
			//.headless()
			.windowSize({width: 1366, height: 768})
			.addArguments(
				'--no-sandbox',
				'--disable-web-security',
				'--no-first-run',
				'--disable-default-apps',
				'--ignore-certificate-errors',
				'--bswi',
				'--no-default-browser-check',
				'--use-fake-device-for-media-stream',
				'--use-fake-ui-for-media-stream',
				'--allow-insecure-localhost',
			);
		const driver = await new Builder()
			.forBrowser('chrome')
			.setChromeOptions(options)
			.build();
		return new WebDriverWrapper(driver);
	}

	async createInsecureFirefox(): Promise<WebDriverWrapper> {

		const capabilities = new FirefoxOptions().setAcceptInsecureCerts(true);

		const options = new FirefoxOptions()
			.headless()
			.windowSize({width: 1366, height: 768})
			.addExtensions('./browser-extensions/firefox/allow-cors.xpi')
			.setPreference('xpinstall.signatures.required', false)
			.setPreference('security.fileuri.strict_origin_policy', false)
			.setPreference('media.navigator.streams.fake', true)
			.setPreference('dom.disable_beforeunload', false)
			.addArguments(
				'--no-sandbox',
				'--disable-web-security',
				'--no-first-run',
				'--disable-default-apps',
				'--ignore-certificate-errors',
				'--bswi',
				'--no-default-browser-check',
				'--use-fake-device-for-media-stream',
				'--use-fake-ui-for-media-stream',
			);
		const driver = await new Builder()
			.forBrowser('firefox')
			.setFirefoxOptions(options)
			.withCapabilities(capabilities)
			.build();
		return new WebDriverWrapper(driver);
	}

	static async createBrowser(browserSelected: 'chrome' | 'firefox'): Promise<WebDriverWrapper> {
		return browserSelected === 'firefox' ?
			new WebDriverFactory().createInsecureFirefox() :
			new WebDriverFactory().createInsecureChrome();
	}

}
