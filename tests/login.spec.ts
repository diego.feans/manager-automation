import 'chromedriver';
import 'geckodriver';
import 'selenium-webdriver/chrome';
import 'selenium-webdriver/firefox';
import {ConfigLoader} from '../config/config-loader';
import {Credentials} from '../models/credentials.model';
import {LoginPage} from '../pages/login.page';
import { MainPage } from '../pages/main.page';
import {WebDriverFactory} from '../selenium/web-driver.factory';
import {WebDriverWrapper} from '../selenium/web-driver.wrapper';

describe('e2e/tests/core/login.spec.ts', () => {
	let driver: WebDriverWrapper;
	let loginPage: LoginPage;
	let mainPage: MainPage;
	let admin: Credentials;

	beforeAll(async () => {
		admin = (await ConfigLoader.getConfig()).ADMIN_CREDENTIALS;
		driver = await WebDriverFactory.createBrowser('chrome');
		loginPage = new LoginPage(driver);
	});

	afterAll(async () => driver.quit());

	beforeEach(async () => {
		await loginPage.navigateTo();
	});

	// afterEach(async () => {
	// 	await loginPage.logout();
	// });

	it('should be able to do login with admin credentials', async () => {
		await loginPage.login(admin);
	});
});
