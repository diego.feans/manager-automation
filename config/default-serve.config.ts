import DefaultConfig from './default.config';

export default Object.assign({}, DefaultConfig, {
	WEBPHONE_URI: 'http://localhost:4200',
});
