export interface Credentials {
	username: string;
	password: string;
	domain?: string;
	voicemail_extension?: string;
}
