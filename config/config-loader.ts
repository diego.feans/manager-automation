import { Credentials } from '../models/credentials.model';
import { VoicemailConfigs } from '../models/voicemail.model';
import DefaultConfig from './default.config';

export class ConfigLoader {
	private static config: typeof DefaultConfig;

	static async getConfig(): Promise<typeof DefaultConfig> {
		const config = process.env.E2E_CONFIG || 'default';
		if (!ConfigLoader.config) {
			ConfigLoader.config = (await import(`./${config.toLowerCase()}.config.ts`)).default;
		}
		return ConfigLoader.config;
	}

	// static async getCredentials(username: string): Promise<Credentials> {
	// 	const credentials = (await ConfigLoader.getConfig()).ADMIN_CREDENTIALS;
	// 	const credential = credentials.find((c) => c.username === username);
	// 	if (!credential) {
	// 		throw Error(`No credentials found for user with name: ${username}`);
	// 	}
	// 	return credential;
	// }

	static async getExtendedTimeout(): Promise<number> {
		//const extendedTimeout = (await ConfigLoader.getConfig()).EXTENDED_TIMEOUT;
		return 60000;
	}
}
