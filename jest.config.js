module.exports = {
	verbose: true,
	projects: ['<rootDir>'],
	testMatch: ['**/tests/**/*.spec.[jt]s?(x)'],
	testPathIgnorePatterns: [
		'/(?:production_)?node_modules/',
		'.d.ts$',
		'<rootDir>/test/fixtures',
		'<rootDir>/test/helpers',
		'__mocks__',
	],
	testEnvironment: 'node',
	transform: {
		'^.+\\.[jt]sx?$': 'ts-jest',
	},
	testTimeout: 1000 * 60 * 5,
};
