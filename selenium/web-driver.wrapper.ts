import {By, Condition, Locator, until, WebDriver, WebElement} from 'selenium-webdriver';
import {ConfigLoader} from '../config/config-loader';

export interface WebDriverWrapper extends WebDriver {}

const locate = (s: string | Locator): Locator => (typeof s === 'string' ? By.css(s) : s);

export class WebDriverWrapper {
	static readonly WAIT_UNTIL_TIME = 20000;

	constructor(private driver: WebDriver) {
		return Object.assign(Object.create(driver), {
			waitTimeout: this.waitTimeout.bind(this),
			querySelector: this.querySelector.bind(this),
			querySelectorExtendedTimeout: this.querySelectorExtendedTimeout.bind(this),
			querySelectorReducedTimeout: this.querySelectorReducedTimeout.bind(this),
			querySelectorAll: this.querySelectorAll.bind(this),
			querySelectorAllReducedTimeout: this.querySelectorAllReducedTimeout.bind(this),
			clearSessionStorage: this.clearSessionStorage.bind(this),
			clearLocalStorage: this.clearLocalStorage.bind(this),
		});
	}

	async waitTimeout<T>(
		condition: PromiseLike<T> | Condition<T> | ((driver: WebDriver) => T | PromiseLike<T>),
		message?: string, timeout?: number,
	): Promise<T> {
		return this.driver.wait(condition, timeout, message);
	}

	async querySelector(selector: string | Locator): Promise<WebElement> {
		const timeout = WebDriverWrapper.WAIT_UNTIL_TIME;
		const element = await this.waitTimeout(until.elementLocated(locate(selector)), null, timeout);
		return this.waitElementVisible(element);
	}

	async querySelectorExtendedTimeout(selector: string | Locator): Promise<WebElement> {
		const timeout = await ConfigLoader.getExtendedTimeout();
		const element = await this.waitTimeout(until.elementLocated(locate(selector)), null, timeout);
		return this.waitElementVisible(element);
	}

	async querySelectorReducedTimeout(selector: string | Locator): Promise<WebElement> {
		const timeout = await ConfigLoader.getExtendedTimeout();
		const element = await this.waitTimeout(until.elementLocated(locate(selector)), null, WebDriverWrapper.WAIT_UNTIL_TIME / 5);
		return this.waitElementVisible(element);
	}

	async querySelectorAll(selector: string | Locator): Promise<WebElement[]> {
		const elements = await this.waitTimeout(until.elementsLocated(locate(selector)), null, WebDriverWrapper.WAIT_UNTIL_TIME);
		return Promise.all(elements.map((e) => this.waitElementVisible(e)));
	}

	async querySelectorAllReducedTimeout(selector: string | Locator): Promise<WebElement[]> {
		const elements = await this.waitTimeout(until.elementsLocated(locate(selector)), null, WebDriverWrapper.WAIT_UNTIL_TIME / 5);
		return Promise.all(elements.map((e) => this.waitElementVisible(e)));
	}

	private async waitElementVisible(element: WebElement): Promise<WebElement> {
		await this.waitTimeout(until.elementIsVisible(element));
		return element;
	}

	async clearSessionStorage() {
		await this.driver.executeScript('window.sessionStorage.clear();');
	}

	async clearLocalStorage() {
		await this.driver.executeScript('window.localStorage.clear();');
	}
}
