export interface VoicemailConfigs {
	enabled: boolean;
	extension: string;
}
