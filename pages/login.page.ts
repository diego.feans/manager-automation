import {ConfigLoader} from '../config/config-loader';
import {Credentials} from '../models/credentials.model';
import {WebDriverWrapper} from '../selenium/web-driver.wrapper';

export class LoginPage {
	constructor(private driver: WebDriverWrapper) {}

	static async createLoggedInPage(driver: WebDriverWrapper, credentails: Credentials) {
		const page = new LoginPage(driver);
		await page.navigateTo();
		await page.login(credentails);
		return page;
	}

	async navigateTo(url?: string) {
		const rootUrl = url || (await ConfigLoader.getConfig()).WEBPHONE_URI;
		await this.driver.get(rootUrl);
	}

	async isLoginPage() {
		await this.driver.querySelector('[data-test=login-page]');
	}

	async login(credentials: Credentials) {
		await this.doLogin(credentials);
		//await this.isUserLogged();
	}

	async wrongLogin(credentials: Credentials) {
		await this.doLogin(credentials);
		await this.isLoginPage();
		await this.checkLoginErrorMessage();
	}

	async isUserLogged() {
		await this.driver.querySelector('[data-test=main-page]');
	}

	private async checkLoginErrorMessage() {
		await this.driver.querySelector('[data-test=login-error-message]');
	}

	async logout() {
		await this.driver.clearLocalStorage();
		await this.driver.clearSessionStorage();
	}

	private async doLogin(credentials: Credentials) {
		const [usernameInput, passwordInput, loginButton] = await Promise.all([
			this.driver.querySelector('[data-placeholder=Username]'),
			this.driver.querySelector('[data-placeholder=Password]'),
			this.driver.querySelector('[data-test=login-btn]'),
		]);
		await usernameInput.sendKeys(credentials.username);
		await passwordInput.sendKeys(credentials.password);
		await loginButton.click();
	}

	async logoutUsingSettings() {
		const settingsButton = await this.driver.querySelector('[data-test=open-settings]');
		await settingsButton.click();
		const powerOffButton = await this.driver.querySelector('[data-test=sippo-power-off]');
		await powerOffButton.click();
		const acceptButton = await this.driver.querySelector('[data-test=popup-accept-btn]');
		await acceptButton.click();
	}

	async getLoginToken() {
		const token = await this.driver.executeScript(
			`return window.localStorage.getItem('session_token')`,
		);
		return token as string;
	}
}
